<?php include_once("conn.php"); 

?>
<doctype html>
<head>
<title>Product List</title>
<link rel="stylesheet" href="style.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
<form action="" method="post">
<h1 id="patxt" class="d-inline">Product List</h1>
<input type="submit" class="btn btn-outline-warning" value="MASS DELETE" name="massdelete" id="delete-product-btn" style="float:right;margin-top:2%;margin-left:20px;">
<a href="add-product" class="btn btn-outline-info" name="ADD" id="ADD" style="float:right;margin-top:2%;">ADD</a>
<hr>
<div class="row">
<?php
$row= "select * from product";
$result = mysqli_query($mysqli,$row);
while($row = mysqli_fetch_assoc($result)){
?>


<div class="col-sm-4 border border-dark pt-2 mt-2">
<input type="checkbox" name="checkbox[]" id="delete-checkbox" value="<?php echo $row["sku"]; ?>" >
 <h5 class="text-center"><?php echo $row["sku"]; ?></h5>
 <h5 class="text-center"><?php echo $row["name"]; ?></h5>
 <h5 class="text-center"><?php echo $row["price"]; ?> &dollar;</h5>
 <?php if($row["type"] == "DVD"){ ?>
 <h5 class="text-center">Size: <?php echo $row["size"]; ?> MB</h5>
 <?php  } ?>
 <?php if($row["type"] == "furniture"){ ?>
 <h5 class="text-center">Dimension: <?php echo $row["height"]; echo "x"; echo $row["width"]; echo "x"; echo $row["length"]; ?> </h5>
 <?php  } ?>
 <?php if($row["type"] == "book"){ ?>
 <h5 class="text-center">Weight: <?php echo $row["weight"]; ?> KG</h5>
 <?php  } ?>
 </div>

 <?php } ?>
 </div> 
 </form>
</div> <!-- End Container div -->

<?php
// Check if delete button active, start this 

if(isset($_POST['massdelete']))
{
    $checkbox = $_POST['checkbox'];

for($i=0;$i<count($checkbox);$i++){

$del_id = $checkbox[$i];
$sql = "DELETE FROM product WHERE sku='$del_id'";
$result = mysqli_query($mysqli,$sql)
or die('Error querying database');
}
// if successful redirect to delete_multiple.php 
if($result){
echo "<meta http-equiv=\"refresh\" content=\"0;URL=index\">";
}
 }

mysqli_close($mysqli);

?>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<!-- BOOTSTRAP -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.esm.min.js" integrity="sha384-pXJyILVSfKOB4xKYbM0dJr+oH4iVvo4s7mWbiTHe6LSxd38hl16DMj6AOJyy2Wcz" crossorigin="anonymous"></script>
<!-- Local js -->
<script src="style.js"></script>
</body>
</html>