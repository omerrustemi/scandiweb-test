$("#seeAnotherField").change(function() {
    if ($(this).val() == "DVD") {
      $('#otherFieldDiv').show();
      $('#otherField').attr('required', 'Please, submit required data');
      $('#otherField').attr('data-error', 'Please, submit required data');
    } else {
      $('#otherFieldDiv').hide();
      $('#otherField').removeAttr('required');
      $('#otherField').removeAttr('data-error');
    }
  });
  $("#seeAnotherField").trigger("change");
  
  $("#productType").change(function() {
    if ($(this).val() == "DVD" &&  $('#otherFieldGroupDiv2').hide() &&  $('#otherFieldGroupDiv3').hide()) {
      $('#otherFieldGroupDiv1').show();
      $('#size').attr('required', 'Please, submit required data');
      $('#size').attr('data-error', 'Please, submit required data');
      $('#otherFieldGroupDiv').show();
    }else if ($(this).val() == "furniture" &&  $('#otherFieldGroupDiv1').hide() &&  $('#otherFieldGroupDiv3').hide()) {
        $('#otherFieldGroupDiv2').show();
        $('#height').attr('required', 'Please, submit required data');
        $('#height').attr('data-error', 'Please, submit required data');
        $('#width').attr('required', 'Please, submit required data');
        $('#width').attr('data-error', 'Please, submit required data');
        $('#Length').attr('required', 'Please, submit required data');
        $('#Length').attr('data-error', 'Please, submit required data');
        $('#otherFieldGroupDiv').show();
      }else if ($(this).val() == "book" &&  $('#otherFieldGroupDiv1').hide() &&  $('#otherFieldGroupDiv2').hide()) {
        $('#otherFieldGroupDiv3').show();
        $('#otherFieldGroupDiv').show();
        $('#Weight').attr('required', 'Please, submit required data');
        $('#Weight').attr('data-error', 'Please, submit required data');
        // } else if($(this).val() == "default") {
      // $('#otherFieldGroupDiv').hide();
      // $('#size').removeAttr('required');
      // $('#size').removeAttr('data-error');
      // $('#height').removeAttr('required');
      // $('#height').removeAttr('data-error');
      // $('#width').removeAttr('required');
      // $('#width').removeAttr('data-error');
      // $('#Weight').removeAttr('required');
      // $('#Weight').removeAttr('data-error');
    }else if ($(this).val() == "default" &&  $('#otherFieldGroupDiv1').hide() &&  $('#otherFieldGroupDiv2').hide() &&  $('#otherFieldGroupDiv3').hide()) {
        $('#otherFieldGroupDiv').show();
        $('#size').removeAttr('required');
        $('#size').removeAttr('data-error');
        $('#height').removeAttr('required');
        $('#height').removeAttr('data-error');
        $('#width').removeAttr('required');
        $('#Length').removeAttr('data-error');
        $('#Weight').removeAttr('required');
        $('#Weight').removeAttr('data-error');
    }
  });
  $("#productType").trigger("change");