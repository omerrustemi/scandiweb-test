
<?php
include_once("conn.php");
if(isset($_POST['save'])){
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $size = $_POST['size'];
    $height = $_POST['height'];
    $width = $_POST['width'];
    $length = $_POST['length'];
    $weight = $_POST['weight'];
    $type= $_POST['productType'];
$results = mysqli_query($mysqli, "select sku from product where sku='$sku'");
if(mysqli_num_rows($results)>0){
    echo "<p class='text-danger' style='padding-left:50%;font-size:24px;'>This SKU id exist</p>";
}else{

  $result = mysqli_query($mysqli, "INSERT INTO product(sku,Name,price,size,height,width,length,weight,type) VALUES('$sku','$name','$price','$size','$height','$width','$length','$weight','$type')");
  header("Location: index");  
}
}else{
    
}  

?>
<!doctype html>
<head>
<title>Product Add</title>
<link rel="stylesheet" href="style.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<div class="container w-70">
<form action="" method="post" id="product_form" enctype="multipart/form-data" >
<h1 id="patxt" class="d-inline">Product Add</h1>
<a href="index" class="btn btn-outline-warning" style="float:right;margin-top:2%;margin-left:20px;">Cancel</a>
<button class="btn btn-outline-info" name="save" style="float:right;margin-top:2%;">Save</button>
<hr>
<div class="form-group row">
<label for="sku" class="col-sm-1 col-form-label">SKU</label>
<div class="col-sm-6">
<input type="text" class="form-control w-50" name="sku" id="sku" placeholder="SJHDSHAUAHSI" maxlength="12" required ><br/>
</div>
</div>

<div class="form-group row">
<label for="name" class="col-sm-1 col-form-label">Name</label>
<div class="col-sm-6">
<input type="text" class="form-control w-50" name="name" id="name" placeholder="Name" required><br/>
</div>
</div>

<div class="form-group row">
<label for="price" class="col-sm-1 col-form-label">Price ($)</label>
<div class="col-sm-6">
<input type="number" class="form-control w-50" name="price" id="price" placeholder="Price ($)" required>
</div>
</div>
<br/>

<div class="form-group row">
<label for="switcher" class="col-sm-2 col-form-label">Type Switcher</label>
<div class="col-sm-2">
    <select class="form-control w-100" id="productType" value="productType" name="productType"  required>
          <option value ="default" selected>Type Switcher</option>
          <option value="DVD">DVD-disc</option>
          <option value="furniture">Furniture</option>
          <option value="book">Book</option>
    </select>
</div>
</div> <!-- End Switcher Selector -->
<br/>
<div class="border border-black w-50" id="otherFieldGroupDiv">
    <div class="form-group row" id="otherFieldGroupDiv1">
        <label for="size" class="col-sm-4 col-form-label" >Size (MB)</label>
        <div class="col-sm-6">
        <input type="number" class="form-control w-100" id="size" name="size" >
    </div>
    <p id="size" class="text-danger">Please provide size in Megabyte*</p>
          </div>
      <div class="form-group row" id="otherFieldGroupDiv2">
      <label for="height"  class="col-sm-4 col-form-label">Height (CM)</label>
      <div class="col-sm-6">
        <input type="number" class="form-control w-100 mt-2" id="height" name="height" >
        
       </div>
       <label for="width"  class="col-sm-4 col-form-label">Width (CM)</label>
      <div class="col-sm-6">
        <input type="number" class="form-control w-100 mt-2" id="width" name="width" >
       </div>
       <label for="length"  class="col-sm-4 col-form-label">Length (CM)</label>
      <div class="col-sm-6">
        <input type="number" class="form-control w-100 mt-2" id="length"name="length" >
       </div>
       <p id="length" class="text-danger">Please provide dimensions in  HxWxL Format*</p>
            </div>
      <div class="form-group row" id="otherFieldGroupDiv3">
      <label for="weight"  class="col-sm-4 col-form-label">Weight (KG)</label>
      <div class="col-sm-6">
        <input type="number" class="form-control w-100 mt-2" id="weight" name="weight">
   
       </div>
       <p id="weight" class="text-danger">Please provide Weight in Kilogram*</p>
          </div>

  </div> <!-- End otherFieldGroupDiv -->
</form> <!-- End Form -->
</div> <!-- End Container -->
<!-- JQ -->
<hr>
<p class="text-center">Scandiweb Test assignment</p>
<!-- Input required -->
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Please, submit required data");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})
</script>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<!-- BOOTSTRAP -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.esm.min.js" integrity="sha384-pXJyILVSfKOB4xKYbM0dJr+oH4iVvo4s7mWbiTHe6LSxd38hl16DMj6AOJyy2Wcz" crossorigin="anonymous"></script>
<!-- Local js -->
<script src="style.js"></script>
</body>
</html>